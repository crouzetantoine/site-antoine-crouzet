# ECE 1ère année


---

## Cours 

Vous trouverez ci-dessous les documents destinés aux élèves de CPGE ECE 1ère année, donné lors de l’année scolaire 2020-2021.

!!! attention

    Les cours présentés ici ne sont pas conformes avec la réforme 2021.

Les cours contiennent également la feuille de TD.

!!! info

    N’hésitez pas à signaler toute coquille, remarque, ou approfondissement éventuels.

<figure markdown>
[:fontawesome-brands-gitlab: Cours ECE1 2020-2021](https://forge.apps.education.fr/cpge/ece1-2020-2021){ .md-button .md-button--primary target=_blank}
</figure>

## Sujets ECE1

Vous trouverez ci-dessous les sujets, corrigés et éventuels rapports des concours ECE.

<figure markdown>
| Année | Sujet | Corrigé | Rapport |
|:-----:|:-----:|:-------:|:-------:|
| 2016  | [EDHEC E](/pdf/ece1/2016edhec.pdf){target=_blank} | [EDHEC E](/pdf/ece1/2016edhec_c.pdf){target=_blank} | |
| 2016  | [EML E](/pdf/ece1/2016eml.pdf){target=_blank} | [EML E](/pdf/ece1/2016eml_c.pdf){target=_blank} | [EML E](/pdf/ece1/2016eml_r.pdf){target=_blank} |
| 2016  | [HEC E](/pdf/ece1/2016hec.pdf){target=_blank} | [HEC E](/pdf/ece1/2016hec_c.pdf){target=_blank} | |
| 2016  | [ESSEC 2 E](/pdf/ece1/2016essec2.pdf){target=_blank} | [ESSEC 2 E](/pdf/ece1/2016essec2_c.pdf){target=_blank} ||
| 2017  | [Ecricome E](/pdf/ece1/2017ecricome.pdf){target=_blank} | [Ecricome E](/pdf/ece1/2017ecricome_c.pdf){target=_blank} | [Ecricome E](/pdf/ece1/2017ecricome_r.pdf){target=_blank}|
| 2017  |  | [EDHEC E](/pdf/ece1/2017edhec_c.pdf){target=_blank} | | 
| 2017  | [EML E](/pdf/ece1/2017eml.pdf){target=_blank} | [EML E](/pdf/ece1/2017eml_c.pdf){target=_blank} | |
</figure>
