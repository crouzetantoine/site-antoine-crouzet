# Un CV très partiel
---

### Enseignement  principal

<figure markdown>
| Année | Position |
| ----- | -------- |
|2024- | *Lycée Carnot, Paris*. Professeur de mathématiques et informatique en <font color=green>CPGE PC</font>.| 
|2021-2024 | *Lycée Carnot, Paris*. Professeur de mathématiques et informatique en <font color=blue>CPGE ECG</font> (mathématiques approfondies), première année.| 
|2021-2023 | *Lycée Condorcet, Paris*. Professeur d'informatique (option informatique) en <font color=green>CPGE MP/MP*</font>.|
| 2019-2021 | *Lycée Rabelais, Saint-Brieuc*. Professeur de mathématiques et informatique en <font color=blue>CPGE ECE</font>, 1ère année, et en IPT.|
| 2016-2019 | *Lycée le Dantec, Lannion*. Professeur de mathématiques et informatique en <font color=green>CPGE ATS</font>.|
| 2013-2016 | *Lycée La Folie Saint James, Neuilly-sur-Seine*. Professeur de mathématiques et informatique en <font color=blue>CPGE ECE</font>, 1ère année.|
| 2009-2013 | *Lycée Camille Saint-Saens, Deuil-la-Barre*. Professeur de mathématiques et d'ISN en lycée.|
</figure>

### Autres enseignements

<figure markdown>
| Année | Position |
| ----- | -------- |
| 2016-2021 | *ENSSAT* (école d'ingénieurs). Chargé de CM, TD et TP en mathématiques en 1ère et 2ème année. |
</figure>

### Jury et concours

* **Concours de recrutement**

<figure markdown>
| Année     | Position |
|---|---|
| 2020-2024 | Membre du directoire du jury du *CAPES* NSI               |
| 2020-2024 | Membre du jury du *CAPES interne* de mathématiques        |
| 2018-2022 | Membre du jury de l'*Agrégation interne* de mathématiques | 
| 2014-2018 | Membre du jury du *CAPES* de mathématiques                |
</figure>
* **Concours de prépas scientifiques**
<figure markdown>
| Année     | Position |
|---|---|
| 2024- | Correcteur de l'Option Informatique (MP) à *Centrale-Suppélec*.|
| 2019-2022 | Correcteur de l'IPT à *Centrale-Supélec*.|
| 2019 | Examinateur au concours *Mines-Ponts*.|
| 2017- | Examinateur au concours *CMT Série 2*.|
</figure>
* **Concours de prépas économiques**
<figure markdown>
| Année     | Position |
|---|---|
| 2016-2018 | Correcteur au concours *BCE* (épreuve EDHEC).|
| 2015| Correcteur au concours *Ecricome*.|
</figure>

* **Autres concours**
<figure markdown>
| Année     | Position |
|---|---|
| 2018-2022 | Membre du jury national des Olympiades de mathématiques |
| 2014-2017 | Correcteur au concours d'entrée à *Science Po*.|
</figure>

### Autres choses

* Représentant des Mathématiques - voie générale de l'[APHEC](https://aphec.fr) entre octobre 2020 et mai 2021.
* Responsable sur l'année soclaire 2020--2021 de l'atelier mathématiques *MATh.en.JEANS* de Saint-Brieuc.
* Représentant académique de l'[UPS](https://prepas.org) en 2019.
