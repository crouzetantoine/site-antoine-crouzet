# Les concours en CPGE MPI

Vous trouverez ci-dessous, mis à jour autant que possible, les épreuves officielles des concours de CPGE scientifiques pour les étudiants de la filière MPI.

_Dernière mise à jour : 18 novembre 2022_

## E.N.S.

### Paris-Saclay (officiel)

Concours unique info-MPI :

#### Épreuves écrites d'admissibilité

* Composition d'informatique (info C, durée : 4 h ; coefficient 5).
* Composition d'informatique fondamentale (durée : 4 h ; coefficient 5).
* Composition de mathématiques (maths C, durée : 4 h ; coefficient 5).
* Français (durée : 4 h ; coefficient 2 uniquement à l'admission).
* Langue vivante étrangère (durée : 4 h ; coefficient 1,5 uniquement à l'admission) portant, au choix du candidat, sur l'une des langues vivantes étrangères suivantes : allemand, anglais, arabe, espagnol.

#### Épreuves pratiques et orales d'admission

* Interrogation d'informatique fondamentale (coefficient 5).
* Épreuve pratique d'algorithmique et de programmation (coefficient 5).
* Interrogation de mathématiques (coefficient 5).
* Épreuve de langue vivante étrangère (coefficient 1,5).
* Travaux d'initiative personnelle encadrés - Tipe (coefficient 3).

## Mines-Ponts (non officiel)

#### Épreuves écrites d'admissibilité

* Mathématiques 1 (3h, coefficient 4)
* Mathématiques 2 (4h, coefficient 5)
* Informatique 1 (3h, coefficient 3)
* Informatique 2 (4h, coefficient 4)
* Physique 1 (3h, coefficient 3)
* Physique 2 (3h, coefficient 3)
* Français (3h, coefficient 5)
* Langue vivante (1h30, coefficient 3)

#### Épreuves orales d'admission

* Épreuve de Mathématiques (coefficient 11)
* Épreuve de Physique (coefficient 7)
* Épreuve d’Informatique (coefficient 6)
* Épreuve d’évaluation des TIPE (coefficient 6)
* Épreuve de français (coefficient 6)
* Épreuve de langue anglaise (coefficient 5)


## Centrale-Supélec (non officiel)

#### Épreuves écrites d'admissibilité

* Mathématiques 1
* Mathématiques 2
* Physique 1
* Physique 2
* Informatique 
* Français
* Anglais

#### Épreuves pratiques et orales d'admission

* Langue obligatoire
* TP informatique
* TP physique-chimie
* TIPE
* Mathématiques 
* Mathématiques-informatique 
* Physique-chimie
* Langue facultative

## CCINP (officiel)

#### Épreuves écrites d'admissibilité

* Mathématiques 1 (4h, coefficient 11)
* Mathématiques 2 (4h, coefficient 11)
* Physique-Chimie (4h, coefficient 11)
* Informatique (4h, coefficient 12)
* Français-Philo (4h, coefficient 9)
* Langue vivante A (3h, coefficient 4)
* Langue vivante B (facultativ, 1h, coefficient 2)

#### Épreuves pratiques et orales d'admission

* Mathématiques (1h, coefficient 8)
* Physique-Chimie (1h, coefficient 8)
* Informatique (1h, coefficient 10)
* Langue vivante (1h, coefficient 6)
* Informatique (30mn), coefficient 8)
