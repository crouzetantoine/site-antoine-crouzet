# L'Education 

On le lit, on l'entend, on le voit : rien ne va à l'Education Nationale. Mais au lieu de se poser et de réflechir, avec l'ensemble des intervenants, la réaction est plutôt de "réformer", un peu au hasard, selon le Ministre en place. Cela fait des années que des réformes sont imposées, souvent sans l'avis (ou alors en allant contre) des principaux intéressés.

Depuis des années, en échangeant avec des collègues du primaire, du secondaire, du supérieur, qu'ils soient syndiqués ou non, les points de convergence sont nombreux. Et si, au lieu de réformer un niveau au hasard, on utiliserait les différents résultats et constations pour réformer en profondeur et en cohérence les différents niveaux ?

!!! remarque "Disclaimer"
    
	Ce qui est indiqué dans ce texte est une réflexion personnelle, qui repose sur les échanges avec de nombreux acteurs du monde éducatif. Il n'a pas vocation à être "la vérité", 
	mais est un résumé de proposition. Tout est critiquable, tout est amendable, pour une Ecole qui remplit pleinement son rôle.


## Choc des fondamentaux

La constation la plus importante, et peut être la plus inquiétante, est celle faite en sixième. Le niveau actuel est élève qui entre en 6ème est inquiétant dans les deux fondamentaux : le français et les mathématiques. Plus précisemment :

- **Les bases de la grammaire et de la conjugaison ne sont pas acquises**. Ce qui était une norme il y a une trentaine d'année ne l'est plus. Selon les collèges, plus de la moitié des élèves ne savent pas conjuguer au présent, n'accordent pas en genre et nombre (voire ne connaissent pas les règles).
- **L'écriture est difficile**. Les élèves n'aiment pas, ne veulent pas écrire et ont des difficultés de graphie. 
- **La lecture est lente**. Le nombre de mots lus par minute, qui est un repère (certes indicatif) a grandement diminué en 20 ans. 
- **Les bases de calcul de mathématiques ne sont pas acquises**. Les additions, soustractions d'entiers (anciennement appelés "calcul mental"), les multiplications posées sont difficiles, et prennent du temps.

Pour résumer, les fondamentaux du socle de compétence ne sont pas acquis, ou mal. Dès lors que ces fragilités sont présentes, il est très difficile de construire derrière des raisonnements plus évolués, des phrases syntaxiquement compliquées, et donc d'étudier des oeuvres.

Sans rentrer dans le "c'était mieux avant", constatons que les apprentissages du primaire ne sont pas consolidés à l'entrée au collège, ce qui va complexifier les apprentissages ultérieurs. Un élève mauvais lecteur sera handicapé en français, mais aussi dans toutes les matières; un élève qui a du mal à calculer, ou à comprendre les bases de logique, sera handicapé en mathématiques mais aussi dans les autres matières scientifiques, et littéraires (la logique est transversale).

Quand on regarde les programmes des cycles 2 (CP-CE1-CE2) et cycles 3 (CM1-CM2-6e), on constate qu'un professeur des écoles doit enseigner le français, les mathématiques, mais aussi de nombreuses autres disciplines (science, histoire, anglais, arts, sports) dans un volume horaire contraint. De fait, le temps alloué au français et aux mathématiques est réduit. Or, sans ces fondamentaux, la suite du parcours de l'élève sera compliqué.

Des professeurs des écoles que j'ai rencontré m'expliquaient qu'avec le programme qu'ils avaient, français et mathématiques pouvaient ne représenter qu'une heure et demi par jour du volume horaire de travail, parfois moins. Sans nier l'intérêt de la culture générale et du sport, cela est un frein majeur à l'enseignement.

!!! tip "Proposition 1"

    Rendons, pour le primaire, les fondamentaux vraiment fondamentaux : la lecture, l'écriture, la grammaire, l'orthographe et les concepts mathématiques de bases doivent être la priorité
	du primaire.
	L'enseignement du français et des mathématiques doit représenter 3h au minimum par jour des enseignements.
	
Par ailleurs, l'injonction de ne pas donner de devoir, qui peut s'entendre dans le cadre de l'accompagnement d'élèves en difficultés ou dont les parents sont dans l'incapacité d'accomapgner, entraine une difficulté. C'est par la répétition et le travail (léger, mais régulier) qu'on apprend. Par ailleurs, le saut entre le CM2 (pas ou peu de devoir) et la 6e (devoirs dans chaque discipline ou presque) est trop grand.

!!! tip "Proposition 2"

    Retour du principe des devoirs systématiques, dès le CP, avec un principe de "devoir fait" inclus dans le service des enseignants (par exemple, la dernière demi-heure).
	
L'anglais est à apprendre dès le CP, car les apprentissages sont plus simples à 6 ans. Les professeurs des écoles doivent donc être formés et accompagnés. La culture générale (science, histoire géographie, enseignement civique) peut être réduite au CP, et augmenter au fur et à mesure que les fondamentaux sont acquis. Ils aident à la formation de la culture générale de l'élève (et donc du futur citoyen).

!!! tip "Proposition 3"

    Un exemple de volume horaire cohérent, par semaine :
	
	- français : 9 heures,
	- mathématiques : 7 heures,
	- culture générale : 3 à 6 heures, en fonction du niveau,
	- EPS : 5 heures,
	- devoirs faits : 2 heures.
	
Il est également essentiel que le redoublement, dans les petites classes, soient plus facilement proposés. Certaines études montrent que le redoublement est efficace dès lors qu'il est pris tôt et au moment de la découverte des difficultés.

Un élève qui, à la fin de CP, à de très grosses difficultés de lecture, et d'écriture, doit pouvoir redoubler afin de reprendre, sereinement, les apprentissages qui lui permettront de passer dans les classes supérieures en étant solide sur ses acquis.

## Choc au collège

Le collège a été le centre des attentions depuis la mise en place du "Collège unique" (réforme Haby de 1975). Différents ministres ont voulu marquer le collège de leur nom; outre le collège unique, on peut parler de la réforme de Najat Vallaud-Belkacem de 2015 (celle qu'on appelle "réforme du collège") ou la réforme de Grabriel Attal qui est en cours de mise en place.

Les raisons sont souvent communes : les élèves ont des difficultés au collège, donc on "simplifie" le-dit collège pour qu'ils puissent s'en sortir, avec un taux final au brevet (environ 90% de réussite) qui ne veut plus dire grand chose. Mais plutôt que d'augmenter, pour les matières où les difficultés sont présentes (hétérogénéïté induite par le collège inque), le volume horaire, ou accentuer sur les dédoublement, on préfère "simplifier" l'enseignement (la raison étant plutôt le manque d'enseignant, cf infra).

Faisons simple : en 6e, 4h30 de français et 4h30 de mathématiques sont prévus, dans des classes à plus de 30 (certaines 6e atteignent les 35). Les dédoublements ne sont plus de droits. En 1990, le volume horaire était quasi identique, mais avec effectif à 24 (si cela dépassait, une heure était alloué pour des dédoublements) et surtout, avec un niveau moyen des élèves bien plus conséquent.

Gabriel Attal a donc proposé, dans ces deux disciplines fondamentales, d'imposer des groupes de niveau : chaque élève sera dans un groupe pour ces heures d'enseignement, de "en difficulté" (théoriquement limité à 15, mais aucun texte n'existe), "moyen" et "avancé" (qui, quant à eux, peuvent atteindre les 35 élèves). En plus de stigmatiser certains élèves, la littérature montre que les groupes de niveau ne fonctionnent pas pour les élèves en difficulté. Par ailleurs, pour organiser tout ça, il va falloir alligner des emplois du temps, rendant la chose compliquée pour les Chefs d'Etablissements, mais qui pourra de fait amener des emplois du temps pour les élèves plus étendus, et avec des "trous".

Il ne faut bien sûr ni léser les élèves en difficulté, ni ceux qui pourraient "s'ennuyer" en classe; pour cela, on peut envisager d'autres méthodes que ces groupes de niveaux :

!!! tip "Proposition 4"

    En plus d'un tronc commun de 4h, où le groupe classe reste ensemble, il faut dans ces disciplines, imposer une heure de dédoublement (pour travailler l'oral en français, par exemple,
	ou l'outil numérique en mathématiques) ainsi qu'une heure à petit effectif (18 maximum) de "Soutien ou approfondissement" de la sixième à la troisième. Cela donne 6h par élève dans chaque discipline, dont 2h en 
	effectif réduit, et avec des moyens pour aider les élèves en difficulté, ou de faire monter le niveau de ceux qui s'en sortent bien.
	
Les heures de dédoublement, qui sont actuellement sur les marges des établissements (entendre : chaque discipline doit se battre avec les autres pour récupérer des heures de déoublement) sont pourtant importants dans certaines disciplines, comme les langues (le travail de l'oral) ou les sciences (TP en physique et SVT).

!!! tip "Proposition 5"

    Une heure de dédoublement obligatoire (et donc financée) dans les deux langues, ainsi qu'en Physique-Chimie et en SVT, de la sixième à la troisième.
	
On pourrait arguer que cela augmente le volume horaire des élèves, ce qui est vrai. On peut bien sûr réfléchir à l'organisation horaire dans certaines disciplines s'il le faut. Mais les dédoublements, le travail en petit groupe ponctuel, est essentiel. 

Enfin, concernant le brevet, il faut que celui-ci retrouve sa place de premier examen. Actuellement, le controle continu représente 50% de la note, et les épreuves terminales 50%. Il faudrait, pour que le brevet soit considéré comme un vrai examen, augmenter la part représentée par les épreuves terminales, sans pour autant rendre oblgatoire le brevet pour la poursuite d'étude.

!!! tip "Proposition 6"

    Les épreuves terminales doivent représenter 65% de la note du brevet, et le contrôle continue 35%. L'obtention du brevet n'est pas oblgatoire pour la poursuite d'étude, mais on peut
	envisager une commission de passage, en juillet après les résultats du brevet, pour les élèves souhaitant passer en lycée (général, ou professionnel) afin de s'assurer d'un niveau
	moyen.
	
	Il faut également accepter que le taux d'admission au brevet soit bien inférieur à 90%.
	
## Au lycée

La réforme du lycée de Jean-Michel Blanquer, a introduit différents éléments dont l'objectif est de laisser aux élèves plus de choix dans leur orientation. Quelques années après l'installation, force est de constater que cela induit des difficultés à différents niveaux 

- les emplois du temps, parfois horribles, afin d'aligner les groupes de spécialités;
- les choix, parfois contraints (dans certains établissements) ou originaux, amènent à des difficultés dans le choix de l'orientation du supérieur.


