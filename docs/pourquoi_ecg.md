# Pourquoi faire une CPGE ECG ?

La réforme du lycée a amené une réforme des CPGE économiques. Les deux CPGE existantes en voie générale, ECE et ECS, fusionnent en une seule CPGE ECG, à destination de tout élève ayant un BAC en voie générale. La filière ECT, pour les filières technologiques, évolue également, mais uniquement sur les contenus.

## Voie ECG : les matières
  
Des matières sont communes à toutes les CPGE ECG :

* **Français-Philo**, anciennement *Culture Générale* : 6h de cours (3h de français et 3h de philosophie) dispensant, en première année, des connaissances générales et des méthodes, et en deuxième année, des idées sur un thème particulier. En 2020-2021, il s'agit par exemple de l'*animal*.
* **LV1** et **LV2** : 3h par langue vivante, dont obligatoire au moins une qui sera l'anglais. En général, on conserve les langues étudiées au lycée (il est difficile de commencer une nouvelle langue en CPGE). Grammaire, vocabulaire, et culture au programme.

La réforme des CPGE ECG induit désormais un double choix à faire : un concernant les mathématiques, et l'autre concernant la deuxième matière principale :

* Pour les mathématiques, au choix :
    - **Mathématiques appliquées** : composé de 8h de cours de mathématiques (en général, 6h de cours et 2h de TD), accompagné d'informatique (1h), il s'agit de renforcer les connaissances vues au lycée, et d'en rajouter. Pèle-mèle : l'étude des fonctions, des suites, des probabilités, des graphes, et (nouveauté) de l'algèbre linéaire. Le programme est conséquent mais s'adresse à un publique large. <br/>
    L'informatique, enseigné par le professeur de mathématiques, consiste en un apprentissage approfondie de l'algorithmique, en utilisant le langage Python, appliqué au mathématiques. En deuxième année, en plus d'approfondissement sur Python, on étudie également un autre langage, SQL, qui permet de faire des recherches dans de grandes bases de données (et souvent utile en pratique).
    - **Mathématiques approfondies**  : composé de 9h de cours de mathématiques (en général, 7h de cours et 2h de TD), accompagné d'informatique (1h). Les mathématiques sont plus approfondies et centrées sur d'autres domaines : programme renforcé en analyse et algèbre mais moins d'informatique au programme, qui sont plus utilisé en tant qu'outil.

* Pour l'autre matière :
    - **ESH** pour économie, sociologie et histoire du monde contemporain. Il s'agit de l'étude des phénomènes économiques et sociologiques à travers l'histoire. Plus particulièrement depuis le 18ème siècle, et surtout depuis 1945.
    - **HGGMC** pour histoire, géographie et géopolitique du monde contemporain. Il s’agit d'étudier principalement l’histoire du XXème siècle, ainsi que la géopolotique, c'est-à-dire "l’étude des rivalités de pouvoir sur ou pour du territoire". 
  
**A savoir** : un établissement peut proposer un, deux, trois voire les quatre parcours et vous pouvez candidater sur autant de parcours que vous souhaitez dans un établissement : cela ne comptera que comme un seul voeu.

<figure markdown>
[![CC-BY-NC-SA](img/pourquoi_ecg.jpg){ width="400" }](https://antoine.crouzet.education/img/pourquoi_ecg.jpg){ target=_blank }
</figure>

## Voie ECG : quelles spécialités au lycée ?

Avec la réforme du bac, le choix est laissé libre aux élèges dès la première. Se pose donc légitimement la question du post-bac : quelles spécialités faut-il avoir fait pour venir en CPGE ECG. 

Pour répondre simplement: il suffit d'avoir fait des mathématiques en première (spécialité) et en terminale (spécialité, ou mathématiques complémentaires). Bien sûr, avoir pris SES, HGGSP, et/ou Anglais par exemple permettront d'avoir des bases en arrivant en CPGE, mais ce n'est pas nécessaire. La seule nécessité est d'avoir fait des mathématiques jusqu'en terminale, même s'il ne s'agit que de 3h par semaine. 

Les programmes de mathématiques (appliquées ou approfondies) se basent sur le programme des mathématiques complémentaires - pas de peur donc ! Bien évidemment, si vous envisagez mathématiques approndies, il serait judicieux d'avoir fait la spécialité mathématiques en terminal mais, à nouveau, ce n'est pas nécessaire.


## Voie ECG : non à la censure !

Nombreux sont les élèves qui pensent que la CPGE ECG n'est pas pour eux parce qu'ils ou elles "n'ont pas le niveau". Mais souvent, ce sont les élèves moyens au lycée qui se révèlent en ECG par intérêt pour les matières. 

Une des peurs est également la charge de travail. Oui, la quantité de travail n'est pas négligeable en CPGE, c'est évident. Mais cela nécessite simplement une organisation, à prendre rapidement. 

Vous aimez l'économie ? L'histoire et la géopolitique ? Vous voulez continuer dans un parcours avec des langues ? N'hésitez pas ! Et ne vous censurez pas si votre matière faible est les maths: c'est courant, mais en général, on reprend toutes les bases !

