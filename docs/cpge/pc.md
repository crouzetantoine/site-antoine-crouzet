---
title: PC - 2024-2025
---

# PC - 2024-2025


---

## Cours 

Vous trouverez, sur le gitlab, les cours, avec les exercices, de mathématiques enseigné par Antoine Crouzet en classe de PC au lycée Carnot (Paris). 

<figure markdown>
[:fontawesome-brands-gitlab: Cours PC 2024-2025](https://forge.apps.education.fr/cpge/pc-2024-2025){ .md-button .md-button--primary target=_blank}
</figure>
	
Voici la progression théorique utilisée :

## Progression 2024-2025
