---
hide:
  - navigation
  - toc
---

# Accueil

--- 

Bienvenue sur la page d'accueil du site officiel d'Antoine Crouzet, professeur de mathématiques et informatique en CPGE.

Vous trouverez sur ce site des documents utilisés dans la classe dans laquelle j'enseigne :

* CPGE PC - Mathématiques (depuis septembre 2024)

Vous trouverez égalament d'anciens documents :

* CPGE ECG 1 - Mathématiques approfondies (post reforme 2021)
* CPGE ECE 1ère année (avant réforme 2021)
* CPGE ATS sciences industrielles (conforme au programme actuel)


!!! tip ""

    Professeur de mathématiques et informatique en Classe Préparatoire aux Grandes Ecoles, en CPGE PC.
    <figure mardown>
	![](img/moi.jpg)
	</figure>
	J'ai enseigné les mathématiques en ECG (mathématiques approfondies) 1ère année, ECE 1ère année et en ATS Sciences Industrielles, et l'option informatique en MPSI et MP.


!!! remarque ""

    Tous mes documents publiques sont sous licence CC-BY-NC
    <figure markdown>
       ![CC-BY-NC](img/CC-BY-NC.png){ width="100" }
    </figure>
    Cela signifie que vous êtes libre de : 
	
    * les lire, les utiliser pour travailler, si vous êtes étudiants (et tout retour est accepté !)
    * les utiliser pour préparer vos cours, si vous êtes professeurs (et toute remarque, coquille est même bienvenue !).

    Rappelons (le BY de la licence) que si vous utilisez mon travail comme source (même modifié), vous devez signaler l'origine de votre source (une simple citation de mon nom sur votre site web est largement suffisant).

!!! info ""
    
	Pour toute question, remarque, coquille, n’hésitez pas à me contacter.
	<figure markdown>
	[Envoyer un mail :fontawesome-solid-paper-plane:](mailto:antoine@crouzet.education){ .md-button }
	</figure>

