---
title: ECG - 2023-2024
---

# ECG - 2023-2024


---

## Cours 

Vous trouverez, sur le gitlab, les cours, avec les exercices, de mathématiques approfondies enseigné par Antoine Crouzet au lycée Carnot (Paris). 

<figure markdown>
[:fontawesome-brands-gitlab: Cours ECG 2023-2024](https://forge.apps.education.fr/cpge/ecg-2023-2024){ .md-button .md-button--primary target=_blank}
</figure>
	
Voici la progression théorique utilisée :

## Progression 2023-2024

### Premier semestre

#### Phase 1 

* [ ] Logique et raisonnements 
* [ ] Propriétés des réels 
* [ ] Introduction à l'informatique avec Python
* [ ] Sommes et produits 
* [ ] Fonctions usuelles  
* [ ] Suites 
* [ ] Limites de suites 

#### Phase 2

* [ ] Ensembles et applications 
* [ ] Éléments de combinatoire
* [ ] Probabilités finies
* [ ] Variables aléatoires finies 

#### Phase 3

* [ ] Limites de fonctions 
* [ ] Continuité
* [ ] Dérivabilité
* [ ] Intégration sur un segment 

#### Phase 4

* [ ] Polynômes  
* [ ] Systèmes linéaires 
* [ ] Matrices 
* [ ] Intro aux espaces vectoriels 

### Second semestre

#### Phase 5

* [ ] Applications linéaires

#### Phase 6

* [ ] Analyse asymptotique
* [ ] Formule de Taylor 
* [ ] Développements limités 
* [ ] Séries

#### Phase 7

* [ ] Sommes d'EV
* [ ] Dimension finie
* [ ] Codage matriciel

#### Phase 8

* [ ] Probabilités sur un univers quelconque
* [ ] Variables aléatoires discrètes
* [ ] Couples de VA

#### Phase 9 

* [ ] Intégrales généralisées
* [ ] Convexité
* [ ] Convergence et approximation
