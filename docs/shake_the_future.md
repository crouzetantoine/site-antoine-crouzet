# Cycle de conférence "Shake the Future"

A partir de l'année scolaire 2023, un cycle de conférence est mis en place au lycée Carnot, Paris, nommé « **Shake the future** ».

**_Shake the future_** est un **cycle de conférences** destiné prioritairement aux étudiants des **classes préparatoires aux grandes écoles** et aux **élèves de Première et Terminale du lycée Carnot** (Paris, XVIIe). Il est également ouvert aux enseignants, aux parents d’élèves et aux chefs d’établissements qui en manifesteraient le souhait, dans la limite des places disponibles.
 
**_Shake the future_** a pour objectif **d’aborder les sujets d’actualité, avec ceux qui la font ou qui en sont des spécialistes** : diplomates, universitaires, chefs d’entreprises, cadres de la haute fonction publique, journalistes, intellectuels, artistes, etc. 
 
**_Shake the future_** c’est **45 minutes d’intervention et 15 de questions**, une fois par mois, sur des thèmes aussi différents que _l’intelligence artificielle dans le commerce, les enjeux des élections en Amérique latine, la sécurité nucléaire, la conquête spatiale aujourd’hui, l’impact climatique sur la RSE…_ 
 
**_Shake the future_** a donc aussi pour nature de **permettre aux jeunes de rencontrer des professionnels et à s’interroger sur leurs parcours afin de construire le leur.**


## Calendrier 2023-2024 (en cours de mise à jour)

| Date | Intervenant | Thème | Langue | Info & inscription |
|---    |:-:    |:-:    | :-: | :-: |
| Jeudi 23 novembre 2023, 18h | Frédéric Ermacora<br/><small>General Manager, illycaffè France Belux</small>| Métiers de vente et distribution : évolution et perspectives| Français | [Info et inscription](https://www.linkedin.com/posts/activity-7128445669912014849-A97j) |

<!--
#### Frédéric ERMACORA - Commerce international : enjeux et perspectives chez Illy caffè
 
**Fréderic Ermacora** est aujourd'hui **Country Manager pour la France et le Bénélux** dans l’entreprise Illy dont aucun amateur de café n’ignore le nom. 

Après des **études en France** (MST en commerce international) et **à l’étranger** (Master in International Business à Trieste - Italie), il a **exploré les champs offerts à l’international** (d’abord export manager pour l’Amérique latine et EMEA - Europe, Middle East and Africa; puis Country Manager pour l’Autriche avant de gagner Paris) par cette entreprise italienne familiale (toujours présidée par un membre de la famille Illy). 

**Illy**, c’est une **entreprise de presque 100 ans** (créée en 1933 par Francesco Illy) dans une petite ville du nord est de l’Italie, qui a eu l’ambition de n’être rien moins que le nom de référence de la culture et du café, à travers le monde, précisément dans **147 pays**. Aujourd’hui, ce sont **1230 personnes** dont 45,3% de femmes qui sont à l’origine d’un **chiffre d’affaires de 567,7 millions d’euros** (soit +14% en 1 an). Si Illy est florissante, elle n’en est pas moins **soucieuse de l’éthique et de l’environnement**. **Société B Corp**, elle s’est engagée à respecter des **standards contraignants en termes de performances sociales et environnementales** (+ de 76% de l’énergie utilisée provient des renouvelables, 75,2% des matériaux sont biodégradables et 91,3% des déchets sont récupérés et recyclés). 

_L’intervention de Frédéric Ermacora illustre les **enjeux contemporains du commerce international** et offre des perspectives à tous ceux qui souhaitent intégrer **une école de commerce** (comment, pour quoi faire, quels métiers dans la vente et la distribution demain?), tout en dessinant les contours des **évolutions imminentes, notamment en matière d’intelligence artificielle**._ 
-->