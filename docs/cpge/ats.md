# ATS SII

---

## Cours 

Vous trouverez ci-dessous le cours complet, tel qu’enseigné lors de l’année 2018-2019 (modulo les ajouts potentiels et autres coquilles).

Les cours contiennent également la feuille de TD.

!!! info

    N’hésitez pas à signaler toute coquille, remarque, ou approfondissement éventuels.

<figure markdown>
[:fontawesome-brands-gitlab: Cours ATS 2018-2019](https://forge.apps.education.fr/cpge/ats-2018-2019){ .md-button .md-button--primary target=_blank}
</figure>


